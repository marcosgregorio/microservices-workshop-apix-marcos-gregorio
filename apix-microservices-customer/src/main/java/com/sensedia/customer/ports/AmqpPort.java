package com.sensedia.customer.ports;

import com.sensedia.commons.errors.domains.DefaultErrorResponse;
import com.sensedia.customer.domains.Customer;

public interface AmqpPort {

  void publishCustomerCreation(Customer customer);

  void publishCustomerOperationError(DefaultErrorResponse errorResponse);
}

package com.sensedia.notification.adapters.mappers;

import com.sensedia.notification.adapters.dtos.CustomerAccountCreationDto;
import com.sensedia.notification.domains.Notification;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface NotificationMapper {

    @Mapping(target = "firstName", source = "customer.firstName")
    @Mapping(target = "lastName", source = "customer.lastName")
    Notification toNotification(CustomerAccountCreationDto customerCreationDto);
}

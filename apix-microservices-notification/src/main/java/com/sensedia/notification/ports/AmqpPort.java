package com.sensedia.notification.ports;

import com.sensedia.commons.errors.domains.DefaultErrorResponse;

public interface AmqpPort {

  void notifyNotificationOperationError(DefaultErrorResponse errorResponse);

}
